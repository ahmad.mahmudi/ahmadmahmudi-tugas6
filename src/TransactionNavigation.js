import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Transaction from './screen/TransactionScreen';
import FormulirScreen from './screen/FormulirScreen';
import ReserveasiCodeScreen from './screen/ReserveasiCodeScreen';
import Checkout from './screen/CheckoutScreen';
import Payment from './screen/SuccessPay';

const Stack = createNativeStackNavigator();

export default function TransactionNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="TransactionScreen"
        component={Transaction}></Stack.Screen>
      <Stack.Screen
        name="FormulirScreen"
        component={FormulirScreen}></Stack.Screen>
      <Stack.Screen
        options={{headerShown: true, title: ''}}
        name="ReservasiCode"
        component={ReserveasiCodeScreen}></Stack.Screen>
      <Stack.Screen
        options={{headerShown: true}}
        name="Checkout"
        component={Checkout}></Stack.Screen>
      <Stack.Screen
        name="PaymentScreen"
        component={Payment}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({});
