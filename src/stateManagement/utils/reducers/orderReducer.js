const initialState = {
    detailOrder: []
  };
  
  const orderReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_DATA_ORDER':
        console.log('toko reducer: ADD_DATA')
        return {
          ...state,
          detailOrder: action.data,
        };
      case 'UPDATE_DATA_ORDER':
        console.log('toko reducer: UPDATE_DATA')
        var newData = [...state.detailOrder];
        var findIndex = state.detailOrder.findIndex(value => {
          return value.id === action.data.id;
        });
  
        newData[findIndex] = action.data;
        return {
          ...state,
          detailOrder: newData,
        };
      case 'DELETE_DATA_ORDER':
        var newData = [...state.detailOrder];
        var findIndex = state.detailOrder.findIndex(value => {
          return value.id === action.id;
        });
        newData.splice(findIndex, 1);
        return {
          ...state,
          detailOrder: newData,
        };
      default:
        return state;
    }
  };
  
  
  export default orderReducer