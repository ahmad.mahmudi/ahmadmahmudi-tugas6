const initialState = {
  userData: {
    nama: '',
    email: '',
    nomorHp: '',
    alamat: '',
  },
  isLoggedin: false,
};

const autReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoggedin: true,
        userData: action.data,
      };
    case 'LOGOUT':
      return {
        ...state,
        userData: {
          nama: '',
          email: '',
          nomorHp: '',
          alamat: '',
        },
        isLoggedin: false,
      };
    default:
      return state;
  }
};

export default autReducer;
