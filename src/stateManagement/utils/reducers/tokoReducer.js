const initialState = {
  detailToko: []
};

const tokoReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_TOKO':
      console.log('toko reducer: ADD_DATA')
      return {
        ...state,
        detailToko: action.data,
      };
    case 'UPDATE_DATA_TOKO':
      console.log('toko reducer: UPDATE_DATA')
      var newData = [...state.detailToko];
      var findIndex = state.detailToko.findIndex(value => {
        return value.id === action.data.id;
      });

      newData[findIndex] = action.data;
      return {
        ...state,
        detailToko: newData,
      };
    case 'DELETE_DATA_TOKO':
      console.log('toko reducer: DELETE_DATA')
      var newData = [...state.detailToko];
      var findIndex = state.detailToko.findIndex(value => {
        return value.id === action.id;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        detailToko: newData,
      };
    default:
      return state;
  }
};


export default tokoReducer