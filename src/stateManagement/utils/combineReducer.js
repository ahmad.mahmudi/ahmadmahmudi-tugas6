import { combineReducers } from "redux";
import authReducer from "./reducers/authReducer";
import orderReducer from "./reducers/orderReducer";
import tokoReducer from "./reducers/tokoReducer";
import transaksiReducer from "./reducers/transaksiReducer";

const rootReducer = combineReducers({
    auth: authReducer,
    toko: tokoReducer,
    order: orderReducer,
    transaksi: transaksiReducer
})

export default rootReducer