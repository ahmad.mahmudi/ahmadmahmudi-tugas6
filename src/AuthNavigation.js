import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import LoginScreen from './screen/LoginScreen'

const Stack = createNativeStackNavigator()

export default function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name ='LoginScreen' component={LoginScreen}/>
    </Stack.Navigator>
        );
    }

const styles = StyleSheet.create({})