import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import ProfileScreen from './screen/ProfileScreen';
import EditProfile from './screen/EditProfileScreen';
import FAQ from './screen/FAQScreen';

const Stack = createNativeStackNavigator();

export default function ProfileNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="ProfileScreen"
        component={ProfileScreen}></Stack.Screen>
      <Stack.Screen
        options={{headerShown: true}}
        name="Edit Profile"
        component={EditProfile}></Stack.Screen>
      <Stack.Screen
        options={{headerShown: true}}
        name="FAQ"
        component={FAQ}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({});
