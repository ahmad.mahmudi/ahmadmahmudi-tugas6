import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

const Home = ({navigation, route}) => {
  const {detailToko} = useSelector(state => state.toko);
  const {isLoggedIn, userData} = useSelector(state => state.auth);
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.header}>
          <Image
            style={styles.fotoprofil}
            source={require('../assetss/image/fotoprofil.png')}
          />
          <Image
            style={styles.keranjang}
            source={require('../assetss/icon/Bag.png')}
          />
          <Text style={styles.salam}>Hello, Agil!</Text>
          <Text style={styles.text1}>Ingin merawat dan perbaiki </Text>
          <Text style={styles.text1}>sepatumu? Cari disini</Text>
          <View style={styles.searchtools}>
            <TextInput style={styles.input1} />
            <Image
              style={styles.cari}
              source={require('../assetss/icon/Search.png')}
            />
            <View style={styles.filterbox}>
              <Image
                style={styles.filtericon}
                source={require('../assetss/icon/Filter.png')}
              />
            </View>
          </View>
        </View>

        <View style={styles.body}>
          <View style={styles.kategori}>
            <TouchableOpacity style={styles.boxkategori1}>
              <Image
                style={styles.icon1}
                source={require('../assetss/icon/Sepatu.png')}
              />
              <Text style={styles.texticon}>Sepatu</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.boxkategori2}>
              <View style={styles.circle}>
                <Image
                  style={styles.icon1}
                  source={require('../assetss/icon/Jacket.png')}
                />
              </View>
              <Text style={styles.texticon}>Jaket</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.boxkategori3}>
              <Image
                style={styles.icon1}
                source={require('../assetss/icon/Tas.png')}
              />
              <Text style={styles.texticon}>Tas</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.rekomtext}>
            <Text style={styles.rekom1}>Rekomendasi Terdekat</Text>
            <Text style={styles.rekom2}>View All</Text>
          </View>

          <FlatList
            nestedScrollEnabled
            data={detailToko}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <TouchableOpacity
                style={styles.list1}
                onPress={() =>
                  navigation.navigate('HomeNavigation', {
                    screen: 'StoreDetails',
                    params: {item},
                  })
                }>
                <Image
                  style={styles.listimage}
                  source={require('../assetss/image/list1.png')}
                />
                <View style={styles.keterangan}>
                  <Image
                    style={styles.star}
                    source={require('../assetss/icon/Star.png')}
                  />
                  <Text style={styles.rating}>{item?.rating} ratings</Text>
                  <Text style={styles.namatoko}>{item?.namaToko}</Text>
                  <Text style={styles.alamat}>{item?.alamat}</Text>
                  <View
                    style={{
                      backgroundColor: item?.buka ? 'green' : 'red',
                      borderRadius: 10,
                      marginTop: 16,
                      width: 58,
                      height: 21,
                    }}>
                    <Text style={styles.closetext}>
                      {item?.buka ? 'BUKA' : 'TUTUP'}
                    </Text>
                  </View>
                </View>
                <Image
                  style={styles.favorite}
                  source={require('../assetss/icon/Heart.png')}
                />
              </TouchableOpacity>
            )}
          />

          {/* <TouchableOpacity style={styles.list1}>
            <Image
              style={styles.listimage}
              source={require('../assetss/image/list1.png')}
            />
            <View style={styles.keterangan}>
              <Image
                style={styles.star}
                source={require('../assetss/icon/Star.png')}
              />
              <Text style={styles.rating}>4.8 ratings</Text>
              <Text style={styles.namatoko}>Jack Repair Gejayan</Text>
              <Text style={styles.alamat}>Jl. Gejayan III No.2, Karangasem, Kec. Laweyan . . .</Text>
              <View style={styles.close}>
                <Text style={styles.closetext}>TUTUP</Text>
              </View>
            </View>
            <Image
              style={styles.favorite}
              source={require('../assetss/icon/Heart.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.list2} onPress={() => navigation.navigate('HomeNavigation',{screen:'StoreDetails'})}>
            <Image
              style={styles.listimage}
              source={require('../assetss/image/list2.png')}
            />
            <View style={styles.keterangan}>
              <Image
                style={styles.star}
                source={require('../assetss/icon/Star.png')}
              />
              <Text style={styles.rating}>4.7 ratings</Text>
              <Text style={styles.namatoko}>Jack Repair Seturan</Text>
              <Text style={styles.alamat}>Jl. Seturan Kec. Laweyan . . .</Text>
              <View style={styles.open}>
                <Text style={styles.opentext}>BUKA</Text>
              </View>
            </View>
            <Image
              style={styles.favorite}
              source={require('../assetss/icon/EmptyHeart.png')}
            />
          </TouchableOpacity> */}


        </View>
      </ScrollView>
      <TouchableOpacity
            onPress={() =>
              navigation.navigate('HomeNavigation', {screen: 'Add Store Data'})
            }
            style={{
              backgroundColor: 'red',
              width: 50,
              height: 50,
              alignSelf: 'center',
              borderRadius: 25,
              paddingTop: 10,
              paddingBottom: 10,
              position: 'absolute',
              bottom: 20,
              right: 20,
            }}>
            <Image
              style={{alignSelf: 'center', width: 30, height: 30}}
              source={require('../assetss/icon/add_icon.png')}
            />
          </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  opentext: {
    color: '#11A84E',
    fontWeight: '700',
    textAlign: 'center',
    fontSize: 12,
    lineHeight: 19,
    letterSpacing: 0.5,
  },
  open: {
    marginTop: 16,
    backgroundColor: 'rgba(17,168,78,0.18)',
    width: 58,
    height: 21,
    borderRadius: 10,
  },
  closetext: {
    color: 'white',
    fontWeight: '700',
    textAlign: 'center',
    fontSize: 12,
    lineHeight: 19,
    letterSpacing: 0.5,
  },
  alamat: {
    color: '#D8D8D8',
    fontSize: 9,
    fontWeight: '500',
    marginTop: 3,
  },
  namatoko: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '600',
    marginTop: 3,
  },
  rating: {
    color: '#D8D8D8',
    fontSize: 12,
    marginTop: 4,
  },
  keterangan: {
    marginLeft: 16,
    marginTop: 15,
  },
  star: {
    width: 50,
    height: 8,
  },
  favorite: {
    width: 13,
    height: 12,
    position: 'absolute',
    right: 10,
    top: 10,
  },
  listimage: {
    width: 80,
    height: 121,
    borderRadius: 5,
    marginLeft: 6,
    marginBottom: 6,
    marginTop: 6,
  },
  list2: {
    height: 133,
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 9,
    marginTop: 5,
    flexDirection: 'row',
  },
  list1: {
    height: 133,
    backgroundColor: 'white',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 9,
    flexDirection: 'row',
    marginBottom: 10,
  },
  rekom2: {
    fontSize: 10,
    fontWeight: '500',
    color: '#E64C3C',
    paddingRight: 10,
  },
  rekom1: {
    color: '#0A0827',
    fontSize: 12,
    fontWeight: '600',
    paddingLeft: 5,
  },
  rekomtext: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 20,
  },
  texticon: {
    color: '#BB2427',
    fontSize: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 5,
    marginBottom: -5,
  },
  icon1: {
    width: 45,
    height: 45,
    alignSelf: 'center',
  },
  circle: {
    backgroundColor: '#FFDFE0AA',
    width: 45,
    height: 45,
    borderRadius: 25,
    alignSelf: 'center',
  },
  kategori: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  boxkategori1: {
    backgroundColor: 'white',
    width: 95,
    height: 95,
    borderRadius: 16,
    marginLeft: 20,
    marginRight: 10,
    marginTop: 18,
    justifyContent: 'center',
  },
  boxkategori2: {
    backgroundColor: 'white',
    width: 95,
    height: 95,
    borderRadius: 16,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 18,
    justifyContent: 'center',
  },
  boxkategori3: {
    backgroundColor: 'white',
    width: 95,
    height: 95,
    borderRadius: 16,
    marginLeft: 10,
    marginRight: 20,
    marginTop: 18,
    justifyContent: 'center',
  },
  body: {
    backgroundColor: '#F6F8FF',
    height: 500,
    width: '100%',
  },
  container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  header: {
    height: 280,
    width: '100%',
    backgroundColor: '#FFFFFF',
  },
  fotoprofil: {
    width: 45,
    height: 45,
    borderRadius: 10,
    position: 'absolute',
    top: 56,
    left: 22,
  },
  keranjang: {
    width: 18,
    height: 20,
    position: 'absolute',
    top: 68,
    right: 27,
  },
  salam: {
    color: '#034262',
    fontSize: 15,
    marginTop: 101,
    marginLeft: 22,
  },
  text1: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0A0827',
    marginLeft: 22,
    paddingTop: 5,
  },
  cari: {
    width: 17,
    height: 17,
    position: 'absolute',
    top: 40,
    left: 32,
  },
  input1: {
    backgroundColor: '#F6F8FF',
    borderRadius: 13,
    width: '75%',
    marginLeft: 20,
    marginTop: 25,
    height: 45,
    paddingRight: -10,
  },
  filterbox: {
    backgroundColor: '#F6F8FF',
    width: 45,
    height: 45,
    borderRadius: 13,
    marginTop: 25,
    marginLeft: 15,
  },
  searchtools: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  filtericon: {
    width: 16,
    height: 16,
    margin: 15,
  },
});
