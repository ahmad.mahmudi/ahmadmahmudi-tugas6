import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

export default function EditProfile({navigation}) {
  return (
    <View style={styles.container}>
      <Image
        style={styles.foto}
        source={require('../assetss/image/fotoprofil.png')}
      />
      <View style={styles.editbox}>
        <Image
          style={styles.editlogo}
          source={require('../assetss/icon/Edit.png')}
        />
        <Text style={styles.edittext}>Edit Foto</Text>
      </View>
      <Text style={styles.title}>Nama</Text>
      <TextInput value='Agil Bani' style={styles.input}></TextInput>
      <Text style={styles.title}>Email</Text>
      <TextInput value='gilagil@gmail.com' style={styles.input}></TextInput>
      <Text style={styles.title}>No HP</Text>
      <TextInput value='08123456789' style={styles.input}></TextInput>
      <TouchableOpacity style={styles.button} onPress={()=>navigation.navigate('Profile')}>
        <Text style={styles.buttontext}>Simpan</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 21,
    paddingRight: 21,
  },
  foto: {
    width: 95,
    height: 95,
    alignSelf: 'center',
    marginTop: 31,
  },
  editbox: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 18,
    marginBottom: 55,
  },
  editlogo: {
    width: 24,
    height: 24,
    marginRight: 8,
  },
  edittext: {
    color: '#3A4BE0',
    fontSize: 18,
    fontWeight: '500',
  },
  title: {
    color: '#BB2427',
    fontSize: 12,
    fontWeight: '600',
  },
  input: {
    color: 'black',
    fontSize: 12,
    fontWeight: '300',
    width: '100%',
    backgroundColor: '#F6F8FF',
    borderRadius: 7.5,
    marginTop: 11,
    marginBottom: 27,
    paddingLeft: 10,
  },
  button: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
    backgroundColor: '#BB2427',
    borderRadius: 7.5,
    position: 'absolute',
    alignSelf: 'center',
    bottom: 50,
  },
  buttontext: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '700'
  },
});
