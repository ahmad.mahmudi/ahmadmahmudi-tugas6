import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
  } from 'react-native';
  import React from 'react';
  
  export default function Hasil({navigation}) {
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 650,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('BottomTabNavigation',{screen:"Transaction"})}>
            <Image
              style={styles.closebtn}
              source={require('../assetss/icon/Close.png')}
            />
          </TouchableOpacity>
          <Text style={styles.text1}>Reservasi Berhasil</Text>
          <Image
            style={styles.done}
            source={require('../assetss/icon/CentangHijau.png')}
          />
          <Text style={styles.text2}>
            Pembayaran Telah Berhasil
          </Text>
        </View>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigation.navigate('TransactionsNavigation',{screen: 'ReservasiCode'})}>
          <Text style={styles.btntext2}>Lihat Kode Reservasi</Text>
        </TouchableOpacity>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    text2: {
      width: 320,
      textAlign: 'center',
      alignSelf: 'center',
      marginTop: 55,
      color: '#000',
      fontSize: 18,
      fontWeight: '400',
    },
    done: {
      width: 160,
      height: 160,
      alignSelf: 'center',
      marginTop: 40,
    },
    text1: {
      color: '#11A84E',
      fontSize: 18,
      fontWeight: '700',
      textAlign: 'center',
      marginTop: 130,
    },
    closebtn: {
      width: 24,
      height: 24,
      marginLeft: 16,
      marginTop: 22,
    },
    container: {
      flex: 1,
      backgroundColor: 'white',
    },
    btntext2: {
      color: 'white',
      fontWeight: '700',
      fontSize: 16,
      textAlign: 'center',
    },
    btn: {
      width: '90%',
      backgroundColor: '#BB2427',
      height: 55,
      borderRadius: 8,
      justifyContent: 'center',
      position: 'absolute',
      bottom: 48,
      left: 20,
    },
  });
  