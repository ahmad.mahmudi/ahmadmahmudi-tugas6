import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';

export default function TransactionScreen({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity>
          <Image
            style={styles.backbtn}
            source={require('../assetss/icon/backblack.png')}
          />
        </TouchableOpacity>
        <Text style={styles.headertext}>Transaksi</Text>
      </View>
      <TouchableOpacity onPress={()=>navigation.navigate('TransactionsNavigation',{screen: 'ReservasiCode'})}>
        <View style={styles.list1}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.tanggal}>20 Desember 2020</Text>
            <Text style={styles.jam}>09:00</Text>
          </View>
          <Text style={styles.merek}>New Balance - Pink Abu - 40</Text>
          <Text style={styles.jasa}>Cuci Sepatu</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.reservasi}>
              Kode Reservasi: <Text style={styles.kode}>CS201201</Text>
            </Text>
            <View style={styles.label}>
              <Text style={styles.status}>Reserved</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  status: {
    color: '#FFC107',
    fontSize: 12,
    fontWeight: '400',
    textAlign: 'center',
    marginTop: 2
  },
  label: {
    width: 81,
    height: 21,
    borderRadius: 10,
    backgroundColor: 'rgba(242,156,31,0.16)',
    position: 'absolute',
    right: 7,
    top: 10
  },
  kode: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '700',
    marginTop: 3,
    marginLeft: 10,
  },
  reservasi: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 13,
    marginLeft: 10,
  },
  jasa: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 3,
    marginLeft: 10,
  },
  merek: {
    color: '#201F26',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 13,
    marginLeft: 10,
  },
  jam: {
    color: '#BDBDBD',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 18,
    marginLeft: 11,
  },
  tanggal: {
    color: '#BDBDBD',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 18,
    marginLeft: 10,
  },
  list1: {
    backgroundColor: 'white',
    height: 128,
    width: '95%',
    alignSelf: 'center',
    marginTop: 9,
    borderRadius: 8,
    shadowColor: 'grey',
    shadowOffset: {width: 10, height: 10},
    shadowRadius: 5,
    elevation: 3,
  },
  backbtn: {
    width: 24,
    height: 24,
    marginLeft: 20,
    marginTop: 16,
  },
  headertext: {
    color: '#201F26',
    fontSize: 18,
    fontWeight: '700',
    marginLeft: 10,
    marginTop: 15,
  },
  header: {
    flexDirection: 'row',
    width: '100%',
    height: 56,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {width: 10, height: 10},
    shadowRadius: 5,
    elevation: 5,
  },
  container: {
    backgroundColor: '#F6F8FF',
    flex: 1,
  },
});
