import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

function StoreDetail({navigation, route}) {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.header}>
          <Image
            style={styles.toko}
            source={require('../assetss/image/TokoDetail.png')}
          />
          <TouchableOpacity
            style={styles.backbtn}
            onPress={() => navigation.goBack()}>
            <Image
              style={styles.backbtnimg}
              source={require('../assetss/icon/backwhite.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('Keranjang')}
            style={{position: 'absolute', top: 20, right: 20}}>
            <Image
              style={styles.keranjang}
              source={require('../assetss/icon/Bagwhite.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          <Text style={styles.namatoko}>{route.params.item.namaToko}</Text>
          <Image
            style={styles.star}
            source={require('../assetss/icon/Star.png')}
          />
          <View style={styles.lokasi}>
            <Image
              style={styles.logomap}
              source={require('../assetss/icon/location.png')}
            />
            <Text style={styles.alamat}>{route.params.item.alamat}</Text>
            <Text style={styles.mapbtn}>Lihat Maps</Text>
          </View>
          <View style={styles.jadwal}>
            <View
              style={{
                width: 55,
                height: 21,
                borderRadius: 10,
                backgroundColor: route.params.item.buka ? 'green' : 'red',
                marginLeft: 33,
                marginTop: 13,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 12,
                  fontWeight: '700',
                  letterSpacing: 0.3,
                  textAlign: 'center',
                  paddingTop: 2,
                }}>
                {route.params.item.buka ? 'BUKA' : 'TUTUP'}
              </Text>
            </View>
            <Text style={styles.jam}>
              {route.params.item.jamBuka} - {route.params.item.jamTutup}
            </Text>
          </View>
          <View style={styles.garis}></View>

          <View style={styles.konten}>
            <Text style={styles.deskripsi}>Deskripsi</Text>
            <Text style={styles.lorem}>{route.params.item.deskripsi}</Text>
            <Text style={styles.range}>Range Biaya</Text>
            <Text style={styles.biaya}>
              Rp{route.params.item.biayaMin} - Rp{route.params.item.biayaMax}
            </Text>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                route.params.item.buka
                  ? navigation.navigate(
                      'Formulir Pemesanan' /*, {params: route.params.item}*/,
                    )
                  : alert('TOKO SEDANG TUTUP');
              }}>
              <Text style={styles.buttontext}>Repair Disini</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                const dataToko = route.params.item;
                console.log(dataToko);
                navigation.navigate('HomeNavigation', {
                  screen: 'Add Store Data',
                  params: {dataToko},
                });
              }}>
              <Text style={styles.buttontext}>Ubah data toko</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

export default StoreDetail;

const styles = StyleSheet.create({
  buttontext: {
    paddingBottom: 15,
    paddingTop: 15,
    textAlign: 'center',
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '700',
  },
  button: {
    backgroundColor: '#BB2427',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 10,
    marginTop: 30,
  },
  konten: {
    height: 460,
  },
  biaya: {
    color: '#8D8D8D',
    fontWeight: '500',
    fontSize: 16,
    width: 313,
    marginLeft: 33,
    marginTop: 6,
  },
  range: {
    fontSize: 16,
    fontWeight: '500',
    color: '#201F26',
    marginLeft: 33,
    marginTop: 23,
  },
  lorem: {
    color: '#595959',
    fontWeight: '400',
    fontSize: 14,
    width: 313,
    marginLeft: 33,
    marginTop: 10,
  },
  deskripsi: {
    fontSize: 16,
    fontWeight: '500',
    color: '#201F26',
    marginLeft: 33,
    marginTop: 23,
  },
  garis: {
    backgroundColor: '#EEE',
    height: 1,
    width: '100%',
    marginTop: 17,
  },
  jam: {
    marginTop: 14,
    marginLeft: 15,
    color: '#343434',
    fontWeight: '700',
    letterSpacing: 0.3,
  },
  jadwal: {
    flexDirection: 'row',
  },
  mapbtn: {
    color: '#3471CD',
    fontSize: 12,
    fontWeight: '700',
    marginLeft: 16,
    marginTop: 16,
  },
  alamat: {
    marginLeft: 7,
    marginTop: 13,
    width: 216,
    fontSize: 10,
    fontWeight: '400',
    color: '#979797',
  },
  logomap: {
    width: 24,
    height: 24,
    marginLeft: 33,
    marginTop: 10,
  },
  lokasi: {
    flexDirection: 'row',
  },
  star: {
    width: 65,
    height: 10,
    marginLeft: 33,
  },
  namatoko: {
    color: '#201F26',
    fontSize: 20,
    fontWeight: '700',
    letterSpacing: 0.3,
    height: 34,
    marginTop: 24,
    marginLeft: 33,
  },
  body: {
    width: '100%',
    height: 540,
    backgroundColor: 'white',
    borderRadius: 20,
    marginTop: -50,
  },
  keranjang: {
    width: 30,
    height: 30,
  },
  backbtnimg: {
    width: 28,
    height: 28,
  },
  backbtn: {
    position: 'absolute',
    top: 20,
    left: 20,
  },
  toko: {
    width: '100%',
    height: 316,
  },
  header: {
    width: '100%',
    height: 316,
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
