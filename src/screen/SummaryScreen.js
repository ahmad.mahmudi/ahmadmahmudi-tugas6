import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import React from 'react';

export default function Ringkasan({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.card1}>
        <Text style={styles.title}>Data Customer</Text>
        <Text style={styles.data}>Agil Bani (0813763476)</Text>
        <Text style={styles.data}>
          Jl. Perumnas, Condong catur, Sleman, Yogyakarta
        </Text>
        <Text style={styles.data}>gantengdoang@dipanggang.com</Text>
      </View>
      <View style={styles.card2}>
        <Text style={styles.title}>Alamat Outlet Tujuan</Text>
        <Text style={styles.data}>Jack Repair Seturan (027-343457)</Text>
        <Text style={styles.data}>Jl. Affandi No 18, Sleman, Yogyakarta</Text>
      </View>
      <View style={styles.card3}>
        <Text style={styles.title}>Barang</Text>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.cardimg}
            source={require('../assetss/image/Sepatu2.png')}
          />
          <View>
            <Text style={styles.cardtext1}>New Balance -Pink Abu - 40</Text>
            <Text style={styles.cardtext2}>Cuci Sepatu</Text>
            <Text style={styles.cardtext2}>Note: -</Text>
          </View>
        </View>
      </View>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('Results')}>
        <Text style={styles.btntext2}>Reservasi Sekarang</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  data: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
    marginBottom: 6,
  },
  title: {
    color: '#979797',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 18,
    marginBottom: 10,
  },
  card1: {
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingBottom: 22,
  },
  card2: {
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingBottom: 22,
  },
  card3: {
    backgroundColor: 'white',
    marginBottom: 12,
    paddingLeft: 26,
    paddingBottom: 37,
  },
  container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
  cardtext2: {
    color: '#737373',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 11,
  },
  cardtext1: {
    color: 'black',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 9,
  },
  cardimg: {
    width: 84,
    height: 84,
    marginRight: 13,
  },
  btntext2: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
    textAlign: 'center',
  },
  btn: {
    width: '90%',
    backgroundColor: '#BB2427',
    height: 55,
    borderRadius: 8,
    justifyContent: 'center',
    position: 'absolute',
    bottom: 48,
    left: 20,
  },
});
