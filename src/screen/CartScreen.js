import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

export default function Keranjang({navigation, route}) {
  const {detailOrder} = useSelector(state => state.order);
  console.log('detailOrder', detailOrder);
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <FlatList
          nestedScrollEnabled
          data={detailOrder}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity onPress={()=> navigation.navigate('Formulir Pemesanan', {...item})}>
              <View style={styles.card}>
                <Image
                  style={styles.cardimg}
                  source={require('../assetss/image/Sepatu2.png')}
                />
                <View>
                  <Text style={styles.cardtext1}>
                    {item.merek} -{item.warna} - {item.ukuran}
                  </Text>
                  <Text style={styles.cardtext2}>
                    {item.jasa1 ? 'Ganti Sol Sepatu' : ' '} |{' '}
                    {item.jasa2 ? 'Jahit Sepatu' : ' '} |{' '}
                  </Text>
                  <Text style={styles.cardtext2}>
                    {item.jasa3 ? 'Repaint Sepatu' : ' '} |{' '}
                    {item.jasa4 ? 'Cuci Sepatu' : ' '}
                  </Text>
                  <Text style={styles.cardtext2}>Note: {item.catatan}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Formulir Pemesanan')}>
          <Image
            style={styles.btnimg}
            source={require('../assetss/icon/Plus.png')}
          />
          <Text style={styles.btntext1}>Tambah Barang</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => navigation.navigate('Summary')}>
        <Text style={styles.btntext2}>Selanjutnya</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  btntext2: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
    textAlign: 'center',
  },
  btn: {
    width: '90%',
    backgroundColor: '#BB2427',
    height: 55,
    borderRadius: 8,
    justifyContent: 'center',
    position: 'absolute',
    bottom: 48,
    left: 20,
  },
  btntext1: {
    color: '#BB2427',
    fontSize: 14,
    fontWeight: '700',
  },
  btnimg: {
    width: 20,
    height: 20,
    marginRight: 8,
  },
  button: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 43,
  },
  cardtext2: {
    color: '#737373',
    fontSize: 12,
    fontWeight: '400',
    marginTop: 11,
  },
  cardtext1: {
    color: 'black',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 9,
  },
  cardimg: {
    width: 84,
    height: 84,
    marginRight: 13,
  },
  card: {
    height: 135,
    width: '90%',
    backgroundColor: 'white',
    alignSelf: 'center',
    marginTop: 10,
    borderRadius: 8,
    flexDirection: 'row',
    paddingLeft: 14,
    paddingTop: 24,
    shadowColor: 'grey',
    shadowOffset: {width: 10, height: 10},
    shadowRadius: 5,
    elevation: 3,
  },
  container: {
    flex: 1,
    backgroundColor: '#F6F8FF',
  },
});
