import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import React from 'react';
import CheckBox from '@react-native-community/checkbox';
import {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';

const Formulir = ({navigation, route}) => {
  const [merek, setMerek] = useState('');
  const [warna, setWarna] = useState('');
  const [ukuran, setUkuran] = useState('');
  const [catatan, setCatatan] = useState('');

  const [centang1, setCentang1] = useState(false);
  const [centang2, setCentang2] = useState(false);
  const [centang3, setCentang3] = useState(false);
  const [centang4, setCentang4] = useState(false);

  const dispatch = useDispatch();
  const {detailOrder} = useSelector(state => state.order);
  console.log('route',route);

  const checkData = () => {
    if (route.params) {
      const data = route.params;
      setMerek(data.merek);
      setWarna(data.warna);
      setUkuran(data.ukuran);
      setCatatan(data.catatan);
      setCentang1(data.jasa1);
      setCentang2(data.jasa2);
      setCentang3(data.jasa3);
      setCentang4(data.jasa4);
    }
  };

  useEffect(() => {
    checkData();
  }, []);

  const updateData = () => {
    const data = {
      id: route.params.dataOrder.id,
      merek: merek,
      warna: warna,
      ukuran: ukuran,
      catatan: catatan,
      jasa1: centang1,
      jasa2: centang2,
      jasa3: centang3,
      jasa4: centang4,
    };
    dispatch({type: 'UPDATE_DATA_ORDER', data});
    navigation.goBack();
  };

  const checkTextInput = () => {
    var submitAble = true;
    if (!merek.trim()) {
      alert('Alamat Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!warna.trim()) {
      alert('Jam Buka Tidak Boleh Kosong');
    }
    if (!ukuran.trim()) {
      alert('Jam Tutup Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!catatan.trim()) {
      alert('Deskripsi Tidak Boleh Kosong');
      submitAble = false;
    }
    return submitAble;
  };

  const storeData = () => {
    if (checkTextInput()) {
      var date = new Date();

      var dataOrder = [...detailOrder];
      const data = {
        id: date.getMilliseconds(),
        merek: merek,
        warna: warna,
        ukuran: ukuran,
        catatan: catatan,
        jasa1: centang1,
        jasa2: centang2,
        jasa3: centang3,
        jasa4: centang4,
      };
      dataOrder.push(data);
      dispatch({type: 'ADD_DATA_ORDER', data: dataOrder});
      navigation.navigate('Keranjang', {params: {dataOrder}});
    }
  };

  const deleteData = () => {
    console.log(route.params.dataOrder);
    dispatch({type: 'DELETE_DATA_ORDER', id: route.params.dataOrder.id});
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <KeyboardAvoidingView>
          <View style={styles.body}>
            <Text style={styles.text}>Merek</Text>
            <TextInput
              value={merek}
              onChangeText={text => setMerek(text)}
              placeholder="Masukkan Merek Barang"
              placeholderTextColor={'#A2A4AB'}
              style={styles.input1}></TextInput>
            <Text style={styles.text}>Warna</Text>
            <TextInput
              value={warna}
              onChangeText={text => setWarna(text)}
              placeholder="Warna Barang, Cth : Merah Putih"
              placeholderTextColor={'#A2A4AB'}
              style={styles.input1}></TextInput>
            <Text style={styles.text}>Ukuran</Text>
            <TextInput
              value={ukuran}
              onChangeText={text => setUkuran(text)}
              keyboardType="number-pad"
              placeholder="Cth : S, M, L / 39,40,41"
              placeholderTextColor={'#A2A4AB'}
              style={styles.input1}></TextInput>
            <Text style={styles.text}>Photo</Text>
            <View style={styles.boxgambar}>
              <TouchableOpacity style={styles.btnimg}>
                <Image
                  style={styles.camera}
                  source={require('../assetss/icon/Camera.png')}
                />
                <Text style={styles.add}>Add Photo</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                value={centang1}
                onValueChange={newValue => setCentang1(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Ganti Sol Sepatu</Text>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                value={centang2}
                onValueChange={newValue => setCentang2(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Jahit Sepatu</Text>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                value={centang3}
                onValueChange={newValue => setCentang3(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Repaint Sepatu</Text>
            </View>
            <View style={styles.checkboxcontainer}>
              <CheckBox
                value={centang4}
                onValueChange={newValue => setCentang4(newValue)}
                style={styles.check}></CheckBox>
              <Text style={styles.checktext}>Cuci Sepatu</Text>
            </View>
            <Text style={styles.text}>Catatan</Text>
            <TextInput
              value={catatan}
              onChangeText={text => setCatatan(text)}
              placeholder="Cth: Ingin ganti sol baru..."
              placeholderTextColor={'#A2A4AB'}
              multiline={true}
              textAlignVertical="top"
              style={styles.input2}></TextInput>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                if (route.params) {
                  updateData();
                } else {
                  storeData();
                }
              }}>
              <Text style={styles.btntext}> {route.params ? 'Change Data' : 'Add to Cart'}</Text>
            </TouchableOpacity>
            {route.params && (
              <TouchableOpacity
                style={[styles.btn1, {backgroundColor: '#dd2c00'}]}
                onPress={deleteData}>
                <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
                  Hapus
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default Formulir;

const styles = StyleSheet.create({
  btn1: {
    marginTop: 20,
    width: '100%',
    backgroundColor: '#43a047',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 15,
  },
  checktext: {
    color: '#201F26',
    fontSize: 14,
    fontWeight: '400',
  },
  check: {
    width: 24,
    height: 24,
    marginRight: 23,
  },
  checkboxcontainer: {
    flexDirection: 'row',
    marginBottom: 23,
  },
  btntext: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
    textAlign: 'center',
  },
  btn: {
    backgroundColor: '#BB2427',
    height: 55,
    borderRadius: 10,
    justifyContent: 'center',
  },
  add: {
    color: '#BB2427',
    marginTop: 14,
    marginBottom: -18,
    textAlign: 'center',
  },
  btnimg: {
    backgroundColor: 'white',
    width: 84,
    height: 84,
    borderRadius: 8,
    justifyContent: 'center',
  },
  boxgambar: {
    backgroundColor: '#BB2427',
    padding: 1,
    borderRadius: 8,
    width: 86,
    height: 86,
    marginTop: 21,
    marginBottom: 43,
  },
  camera: {
    width: 20,
    height: 18,
    alignSelf: 'center',
  },
  input2: {
    backgroundColor: '#F6F8FF',
    height: 92,
    width: '100%',
    borderRadius: 7,
    marginTop: 11,
    marginBottom: 27,
    paddingLeft: 9,
  },
  input1: {
    backgroundColor: '#F6F8FF',
    height: 45,
    width: '100%',
    borderRadius: 7,
    marginTop: 11,
    marginBottom: 27,
    paddingLeft: 9,
  },
  text: {
    color: '#BB2427',
    fontSize: 12,
    fontWeight: '600',
  },
  container: {
    flex: 1,
  },
  body: {
    width: '100%',
    height: 1000,
    backgroundColor: '#FFF',
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: 30,
  },
});
