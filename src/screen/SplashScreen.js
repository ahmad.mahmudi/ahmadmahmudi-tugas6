import { StyleSheet, Text, View, Image } from 'react-native';
import React, {useEffect} from 'react';

const SplashScreen = ({navigation, route}) => {

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('AuthNavigation')
        }, 1500)
     }, [])

  return (
    <View style={styles.container}>
      <Image style={styles.gambar}
      source={require('../assetss/image/Splash.png')}/>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    gambar: {
        width: 150,
        height: 150,
        resizeMode: 'contain',
    },
})

export default SplashScreen;