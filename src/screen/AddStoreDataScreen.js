import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import React from 'react';
import CheckBox from '@react-native-community/checkbox';
import {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useEffect} from 'react';

const AddStoreDataScreen = ({navigation, route}) => {
  console.log('params', route.params);
  // const [idStore, setIdStore] = moment(new Date()).format('YYYYMMDDHHmmssSSS');
  const [alamat, setAlamat] = useState('');
  const [jamBuka, setJamBuka] = useState('');
  const [jamTutup, setJamTutup] = useState('');
  const [deskripsi, setDeskripsi] = useState('');
  const [favorite, setFavorite] = useState('');
  const [buka, setBuka] = useState('');
  const [biayaMin, setBiayaMin] = useState('');
  const [biayaMax, setBiayaMax] = useState('');
  const [rating, setRating] = useState('');
  const [namaToko, setNamaToko] = useState('');

  const {detailToko} = useSelector(state => state.toko);
  const dispatch = useDispatch();

  const [centang1, setCentang1] = useState(false);
  const [centang2, setCentang2] = useState(false);

  const checkData = () => {
    if (route.params) {
      const data = route.params.dataToko;
      setAlamat(data.alamat);
      setJamBuka(data.jamBuka);
      setJamTutup(data.jamTutup);
      setDeskripsi(data.deskripsi);
      setFavorite(data.favorite);
      setBuka(data.buka);
      setBiayaMin(data.biayaMin);
      setBiayaMax(data.biayaMax);
      setRating(data.rating);
      setNamaToko(data.namaToko);
    }
  };

  useEffect(() => {
    checkData();
  }, []);

  const updateData = () => {
    const data = {
      id: route.params.dataToko.id,
      alamat: alamat,
      jamBuka: jamBuka,
      jamTutup: jamTutup,
      deskripsi: deskripsi,
      favorite: favorite,
      buka: centang1,
      biayaMin: biayaMin,
      biayaMax: biayaMax,
      rating: rating,
      namaToko: namaToko,
    };
    dispatch({type: 'UPDATE_DATA_TOKO', data});
    navigation.goBack();
  };

  const checkTextInput = () => {
    var submitAble = true;
    if (!alamat.trim()) {
      alert('Alamat Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!jamBuka.trim()) {
      alert('Jam Buka Tidak Boleh Kosong');
    }
    if (!jamTutup.trim()) {
      alert('Jam Tutup Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!deskripsi.trim()) {
      alert('Deskripsi Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!biayaMin.trim()) {
      alert('Biaya Minimal Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!biayaMax.trim()) {
      alert('Biaya Maximal Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!rating.trim()) {
      alert('Rating Tidak Boleh Kosong');
      submitAble = false;
    }
    if (!namaToko.trim()) {
      alert('Nama Toko Tidak Boleh Kosong');
      submitAble = false;
    }
    return submitAble;
  };

  const storeData = () => {
    if (checkTextInput()) {
      var date = new Date();
      var dataToko = [...detailToko];
      const data = {
        id: date.getMilliseconds(),
        alamat: alamat,
        jamBuka: jamBuka,
        jamTutup: jamTutup,
        deskripsi: deskripsi,
        favorite: favorite,
        buka: centang1,
        biayaMin: biayaMin,
        biayaMax: biayaMax,
        rating: rating,
        namaToko: namaToko,
      };
      dataToko.push(data);
      dispatch({type: 'ADD_DATA_TOKO', data: dataToko});
      navigation.goBack();
    }
  };

  const deleteData = () => {
    console.log(route.params.dataToko);
    dispatch({type: 'DELETE_DATA_TOKO', id: route.params.dataToko.id});
    navigation.goBack();
  };

  return (
    <ScrollView>
      <KeyboardAvoidingView
        style={{
          flex: 1,
          backgroundColor: 'white',
          width: '100%',
          height: '100%',
          padding: 10,
        }}>
        <View>
          <Text style={{color: 'black', fontWeight: '700', fontSize: 22}}>
            {route.params ? 'Change Data' : 'Add New Data'}
          </Text>
          <View style={{width: '89%', alignSelf: 'center', marginTop: 25}}>
            <Text style={{color: 'black', fontWeight: '700'}}>Nama Toko</Text>
            <TextInput
              value={namaToko}
              onChangeText={text => setNamaToko(text)}
              placeholder="Masukan Nama Toko"
              placeholderTextColor={'grey'}
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                marginBottom: 5,
              }}
            />
            <Text style={{color: 'black', fontWeight: '700'}}>Alamat</Text>
            <TextInput
              value={alamat}
              onChangeText={text => setAlamat(text)}
              placeholder="Masukan Alamat"
              placeholderTextColor={'grey'}
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                marginBottom: 5,
              }}
            />
            <Text style={{color: 'black', fontWeight: '700'}}>
              Status Buka Tutup
            </Text>
            <View style={{flexDirection: 'row'}}>
              <View style={styles.checkboxcontainer}>
                <CheckBox
                 
                  value={centang1}
                  onValueChange={newValue => setCentang1(newValue)}
                  style={styles.check}></CheckBox>
                <Text style={{color: 'black'}}>Buka</Text>
              </View>
              {/* <View style={styles.checkboxcontainer}>
                <CheckBox
                  disabled={false}
                  value={centang2}
                  onValueChange={newValue => setCentang2(newValue)}
                  style={styles.check}></CheckBox>
                <Text style={{color: 'black'}}>Tutup</Text>
              </View> */}
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '50%'}}>
                <Text style={{color: 'black', fontWeight: '700'}}>
                  Jam Buka
                </Text>
                <TextInput
                  value={jamBuka}
                  onChangeText={text => setJamBuka(text)}
                  placeholder="Buka"
                  placeholderTextColor={'grey'}
                  style={{
                    borderColor: 'grey',
                    borderWidth: 1,
                    borderRadius: 5,
                    padding: 10,
                    marginBottom: 5,
                  }}
                />
              </View>
              <View style={{width: '50%'}}>
                <Text style={{color: 'black', fontWeight: '700'}}>
                  Jam Tutup
                </Text>
                <TextInput
                  value={jamTutup}
                  onChangeText={text => setJamTutup(text)}
                  placeholder="Tutup"
                  placeholderTextColor={'grey'}
                  style={{
                    borderColor: 'grey',
                    borderWidth: 1,
                    borderRadius: 5,
                    padding: 10,
                    marginBottom: 5,
                  }}
                />
              </View>
            </View>
            <Text style={{color: 'black', fontWeight: '700'}}>
              Jumlah Rating
            </Text>
            <TextInput
              keyboardType="number-pad"
              value={rating}
              onChangeText={text => setRating(text)}
              placeholder="Masukan Rating"
              placeholderTextColor={'grey'}
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                marginBottom: 5,
              }}
            />
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '50%'}}>
                <Text style={{color: 'black', fontWeight: '700'}}>
                  Harga Minimal
                </Text>
                <TextInput
                  keyboardType="number-pad"
                  value={biayaMin}
                  onChangeText={text => setBiayaMin(text)}
                  placeholder="Min."
                  placeholderTextColor={'grey'}
                  style={{
                    borderColor: 'grey',
                    borderWidth: 1,
                    borderRadius: 5,
                    padding: 10,
                    marginBottom: 5,
                  }}
                />
              </View>
              <View style={{width: '50%'}}>
                <Text style={{color: 'black', fontWeight: '700'}} t>
                  Harga Maximal
                </Text>
                <TextInput
                  keyboardType="number-pad"
                  value={biayaMax}
                  onChangeText={text => setBiayaMax(text)}
                  placeholder="Max."
                  placeholderTextColor={'grey'}
                  style={{
                    borderColor: 'grey',
                    borderWidth: 1,
                    borderRadius: 5,
                    padding: 10,
                    marginBottom: 5,
                  }}
                />
              </View>
            </View>
            <Text style={{color: 'black', fontWeight: '700'}}>Deskripsi</Text>
            <TextInput
              value={deskripsi}
              onChangeText={text => setDeskripsi(text)}
              placeholder="Masukan Deskripsi"
              placeholderTextColor={'grey'}
              style={{
                borderColor: 'grey',
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                marginBottom: 5,
              }}
            />
            <Text style={{color: 'black', fontWeight: '700'}}>Gambar Toko</Text>
            <TouchableOpacity
              style={{
                borderWidth: 1,
                borderColor: 'grey',
                padding: 10,
                borderRadius: 6,
              }}>
              <Text style={{textAlign: 'center'}}>Pilih Gambar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (route.params) {
                  updateData();
                } else {
                  storeData();
                }
              }}
              style={{
                backgroundColor: 'black',
                padding: 10,
                alignItems: 'center',
                borderRadius: 8,
                marginTop: 25,
              }}>
              <Text style={{color: 'white', fontWeight: '700'}}>Simpan</Text>
            </TouchableOpacity>
            {route.params && (
              <TouchableOpacity
                style={[styles.btn, {backgroundColor: '#dd2c00'}]}
                onPress={deleteData}>
                <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
                  Hapus
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default AddStoreDataScreen;

const styles = StyleSheet.create({
  btn: {
    marginTop: 20,
    width: '100%',
    backgroundColor: '#43a047',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 15,
  },
  container: {
    flex: 1,
  },
  check: {
    width: 24,
    height: 24,
    marginRight: 23,
  },
  checkboxcontainer: {
    flexDirection: 'row',
    marginBottom: 23,
    width: '50%',
    marginTop: 20,
  },
});
