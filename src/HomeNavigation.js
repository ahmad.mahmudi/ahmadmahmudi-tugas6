import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import HomeScreen from './screen/HomeScreen'
import StoreDetails from './screen/StoreDetailScreen'
import Formulir from './screen/FormulirScreen'
import CartScreen from './screen/CartScreen'
import Summary from './screen/SummaryScreen'
import Results from './screen/ResultScreen'
import AddStoreDataScreen from './screen/AddStoreDataScreen'

const Stack = createNativeStackNavigator()

export default function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name ='HomeScreen' component={HomeScreen}></Stack.Screen>
        <Stack.Screen name ='StoreDetails' component={StoreDetails}></Stack.Screen>
        <Stack.Screen name ='Add Store Data' component={AddStoreDataScreen}/>
        <Stack.Screen options={{headerShown: true}} name ='Formulir Pemesanan' component={Formulir}></Stack.Screen>
        <Stack.Screen options={{headerShown: true}} name ='Keranjang' component={CartScreen}></Stack.Screen>
        <Stack.Screen options={{headerShown: true}} name ='Summary' component={Summary}></Stack.Screen>
        <Stack.Screen name ='Results' component={Results}></Stack.Screen>
    </Stack.Navigator>
        );
    }

const styles = StyleSheet.create({})