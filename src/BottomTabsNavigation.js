import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import HomeNavigation from './HomeNavigation';
import TransactionNavigation from './TransactionNavigation';
import ProfileNavigation from './ProfileNavigation';

const Tab = createBottomTabNavigator();

function TabMenu({
  isFocused,
  label
}){
  const icon = iconType(label)
  return (
    <View style={{
      justifyContent: 'center',
      paddingBottom: 10,
      alignItems: 'center',
    }}>
      <Image source={icon}
      style={{
        width: label === "Transaction" ? 50:20,
        height: label === "Transaction" ? 50:20,
        resizeMode: 'contain',
        tintColor: isFocused ? 'red' : 'grey',  
      }}/>
      <Text style={{
        color: isFocused ? 'red' : 'grey',  
        marginTop: label === "Transaction" ? -19:10
      }}>{label}</Text>
    </View>
  )
}

function iconType(label) {
  if (label === "Home") {
    return require('./assetss/icon/Path.png');
  } else if (label === "Transaction") {
    return require('./assetss/icon/Transaction.png');
  } else {
    return require('./assetss/icon/Profile.png');
}}

function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1, backgroundColor: 'white', height: 80, paddingTop: 20}}>
            <TabMenu
            isFocused= {isFocused}
            label={label}/>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export default function MyTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
    
      }}
      tabBar={props => <MyTabBar {...props} />}>
      <Tab.Screen name="Home" component={HomeNavigation} />
      <Tab.Screen name="Transaction" component={TransactionNavigation} />
      <Tab.Screen name="Profile" component={ProfileNavigation} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({});
